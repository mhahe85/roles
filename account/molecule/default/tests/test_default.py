import os

import testinfra.utils.ansible_runner

testinfra_hosts = testinfra.utils.ansible_runner.AnsibleRunner(
    os.environ['MOLECULE_INVENTORY_FILE']).get_hosts('all')


def test_pip_user_instal_path(host):
    username = host.ansible.get_variables()['username']
    fyle = host.file(
        '/home/{username}/.local/bin'.format(
            username=username
        )
    )

    assert fyle.exists
    assert fyle.user == username
    assert fyle.group == username


def test_user_exists(host):
    username = host.ansible.get_variables()['username']
    user = host.user(username)

    assert user.name == username


def test_user_shell(host):
    username = host.ansible.get_variables()['username']
    user = host.user(username)

    assert "zsh" in user.shell
